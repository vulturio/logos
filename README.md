# Logos e Isotipos Oficiales de Vultur

Los siguientes logos están disponibles para su uso previa autorización.

Para mayor información contactar en hi@vultur.io

## Principal 

![Logo Oficial](LogoVultur_NuevoColor/Circulo_Vultur_Logo.png)

## Polígono

![Logo Oficial](LogoVultur_NuevoColor/Poligono_Vultur_Logo.png)

## Paleta de Colores
#371E75 #28094B
![Paleta](VulturCodigoColores.png)